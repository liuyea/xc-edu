<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
Hello ${name}!
</br>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
       <td>生日</td>
    </tr>
    <#if stus??>
    <#list stus as stu>
        <tr>

            <td>${stu_index+1}</td>
            <td <#if stu.name="小明"> style="background-color: chocolate" </#if>>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.money}</td>
           <td>${stu.birthday?date}</td>

        </tr>
    </#list>
        学生个数：${stus?size}
    </#if>
</table>
</br>
map的值
</br>
姓名：${(stuMap["stu1"].name)!""}</br>
年龄：${(stuMap["stu1"].age)!""}</br>

姓名：${(stuMap.stu1.name)!""}</br>
年龄：${(stuMap.stu1.age)!}</br>
</body>
</html>