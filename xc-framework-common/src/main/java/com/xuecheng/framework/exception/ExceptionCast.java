package com.xuecheng.framework.exception;

import com.xuecheng.framework.model.response.ResultCode;

/**
 * @Classname ExceptionCast
 * @Description TODO
 * @Date 2021/7/24 15:53
 * @Author by YeLiu
 */
public class ExceptionCast {
    public static void cast(ResultCode resultCode){
        throw new CustomException(resultCode);
    }
}
