package com.xuecheng.framework.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @Classname FeignClientInterceptor
 * @Description TODO
 * @Date 2021/8/31 14:47
 * @Author by YeLiu
 */
public class FeignClientInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes servletRequestAttributes=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
       if(servletRequestAttributes!=null){
           //取出header
           HttpServletRequest request = servletRequestAttributes.getRequest();
           Enumeration<String> headerNames = request.getHeaderNames();
           if(headerNames!=null){
               while ( headerNames.hasMoreElements()){
                   String headerName = headerNames.nextElement();
                   String headerValue = request.getHeader(headerName);
                   //传递jwt
                   requestTemplate.header(headerName,headerValue);

               }

           }
       }


    }
}
