package com.xuecheng.manage_media_process.mq;

import com.alibaba.fastjson.JSON;
import com.xuecheng.framework.domain.media.MediaFile;
import com.xuecheng.framework.domain.media.MediaFileProcess;
import com.xuecheng.framework.domain.media.MediaFileProcess_m3u8;
import com.xuecheng.framework.utils.HlsVideoUtil;
import com.xuecheng.framework.utils.Mp4VideoUtil;
import com.xuecheng.manage_media_process.dao.MediaFileRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @Classname MediaProcessTask
 * @Description TODO
 * @Date 2021/8/18 10:37
 * @Author by YeLiu
 */
@Component
public class MediaProcessTask {
    @Autowired
    MediaFileRepository mediaFileRepository;
    @Value("${xc-service-manage-media.ffmpeg-path}")
    String ffmpeg_path;
    @Value("${xc-service-manage-media.video-location}")
    String video_location;

    @RabbitListener(queues = "${xc-service-manage-media.mq.queue-media-video-processor}",containerFactory = "customContainerFactory")
    public void receiveMediaProcessTask(String msg) throws IOException {
        //解析消息内容，拿到MediaId
        Map map = JSON.parseObject(msg, Map.class);
        String mediaId = (String) map.get("mediaId");
        //根据mediaId 查询数据库
        Optional<MediaFile> byId = mediaFileRepository.findById(mediaId);
        if(!byId.isPresent()){
            return;
        }
        MediaFile mediaFile = byId.get();
        String fileType = mediaFile.getFileType();
        if(!fileType.equals("avi")){
            mediaFile.setProcessStatus("303004");
            mediaFileRepository.save(mediaFile);
            return;
        }else {
            mediaFile.setProcessStatus("303001");
            mediaFileRepository.save(mediaFile);
        }

        //将avi转换成MP4
        //avi文件路径
        String video_path=video_location+mediaFile.getFilePath()+mediaFile.getFileName();
        //mp4名称
        String mp4_name= mediaFile.getFileId()+".mp4";
        //MP4文件存放路径
        String Mp4_path=video_location+mediaFile.getFilePath();
        Mp4VideoUtil mp4VideoUtil=new Mp4VideoUtil(ffmpeg_path,video_path,mp4_name,Mp4_path);
        String result = mp4VideoUtil.generateMp4();
        if(result==null || !result.equals("success")){
            mediaFile.setProcessStatus("303003");
            MediaFileProcess_m3u8  mediaFileProcess=new MediaFileProcess_m3u8();
            mediaFileProcess.setErrormsg(result);
            mediaFile.setMediaFileProcess_m3u8(mediaFileProcess);
            mediaFileRepository.save(mediaFile);
            return;
        }
        //将MP4生成m3u8和ts文件
        //mp4的地址
        String mp4_video_path=video_location+mediaFile.getFilePath()+mp4_name;
        //m3u8的名称
        String m3u8_name=mediaFile.getFileId()+".m3u8";
        //m3u8的路径
        String m3u8_path=video_location+mediaFile.getFilePath()+"hls/";
        HlsVideoUtil hlsVideoUtil=new HlsVideoUtil(ffmpeg_path,mp4_video_path,m3u8_name,m3u8_path);
        String tsResult = hlsVideoUtil.generateM3u8();
        if(tsResult==null || !tsResult.equals("success")){
            mediaFile.setProcessStatus("303003");
            MediaFileProcess_m3u8  mediaFileProcess=new MediaFileProcess_m3u8();
            mediaFileProcess.setErrormsg(result);
            mediaFile.setMediaFileProcess_m3u8(mediaFileProcess);
            mediaFileRepository.save(mediaFile);
            return;
        }
        //处理成功
        //获取ts列表
        List<String> ts_list = hlsVideoUtil.get_ts_list();
        MediaFileProcess_m3u8  mediaFileProcess=new MediaFileProcess_m3u8();
        //设置ts列表
        mediaFileProcess.setTslist(ts_list);
        mediaFile.setMediaFileProcess_m3u8(mediaFileProcess);
        //保存fileUrl（就是视频播放的相对路径）
        String fileUrl=mediaFile.getFilePath()+"hls/"+m3u8_name;
        mediaFile.setFileUrl(fileUrl);
        //更新状态
        mediaFile.setProcessStatus("303002");
        mediaFileRepository.save(mediaFile);

    }
}
