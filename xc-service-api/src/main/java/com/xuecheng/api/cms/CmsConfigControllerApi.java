package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.CmsConfig;

/**
 * @Classname CmsConfigControllerApi
 * @Description TODO
 * @Date 2021/7/26 14:30
 * @Author by YeLiu
 */
public interface CmsConfigControllerApi {
    public CmsConfig getmodel(String id);
}
