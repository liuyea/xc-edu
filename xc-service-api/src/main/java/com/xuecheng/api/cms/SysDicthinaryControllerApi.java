package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.system.SysDictionary;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Classname SysDicthinaryControllerApi
 * @Description TODO
 * @Date 2021/8/4 16:34
 * @Author by YeLiu
 */
@Api(value = "数据字典接口",description = "提供数据字典接口的管理、查询功能")
public interface SysDicthinaryControllerApi {
    @ApiOperation(value="数据字典查询接口")
    public SysDictionary getByType(String type);
}
