package com.xuecheng.api.ucenter;

import com.xuecheng.framework.domain.ucenter.ext.XcUserExt;
import io.swagger.annotations.Api;

/**
 * @Classname UcenterControllerApi
 * @Description TODO
 * @Date 2021/8/23 10:29
 * @Author by YeLiu
 */
@Api(value = "用户中心",description = "用户中心管理")
public interface UcenterControllerApi {
    //根据用户账号查询用户信息
    public XcUserExt getUserExt(String username);
}
