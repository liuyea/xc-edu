package com.xuecheng.api.course;

import com.xuecheng.framework.domain.course.*;
import com.xuecheng.framework.domain.course.ext.CourseInfo;
import com.xuecheng.framework.domain.course.ext.CourseView;
import com.xuecheng.framework.domain.course.ext.TeachplanNode;
import com.xuecheng.framework.domain.course.request.CourseListRequest;
import com.xuecheng.framework.domain.course.response.AddCourseResult;
import com.xuecheng.framework.domain.course.response.CoursePublishResult;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Classname CourseControllerApi
 * @Description TODO
 * @Date 2021/8/2 17:45
 * @Author by YeLiu
 */
@Api(value = "课程管理接口",description = "课程管理接口，提供课程的curd")
public interface CourseControllerApi {
    @ApiOperation("课程计划查询")
    public TeachplanNode findTeachPlanList(String courseId);
    @ApiOperation("课程计划添加")
    public ResponseResult addTeachplan(Teachplan teachplan);
    @ApiOperation("查询我的课程列表")
    public QueryResponseResult<CourseInfo> findCourseList(int page, int size, CourseListRequest courseListRequest);
    @ApiOperation("添加我的课程")
    public AddCourseResult addCourseBase(CourseBase courseBase);
    @ApiOperation("查询我的课程基本信息")
    public CourseBase getCourseBaseById(String courseId);
    @ApiOperation("修改我的课程基本信息")
    public ResponseResult updateCourseBase(String courseId,CourseBase courseBase);
    @ApiOperation("查询课程营销信息")
    public CourseMarket getCourseMarketById(String courseId);
    @ApiOperation("修改课程营销信息")
    public ResponseResult updateCourseMarket(String id,CourseMarket courseMarket);
    @ApiOperation("课程视图查询")
    public CourseView courseview(String id);
    @ApiOperation("预览课程")
    public CoursePublishResult preview(String id);
    @ApiOperation("课程发布")
    public CoursePublishResult publish(String id);
    @ApiOperation("保存媒资信息")
    public ResponseResult savemedia(TeachplanMedia teachplanMedia);
}
