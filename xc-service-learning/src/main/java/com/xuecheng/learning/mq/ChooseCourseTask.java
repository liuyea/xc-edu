package com.xuecheng.learning.mq;

import com.alibaba.fastjson.JSON;
import com.xuecheng.framework.domain.task.XcTask;
import com.xuecheng.framework.model.response.ResponseResult;
import com.xuecheng.learning.config.RabbitMQConfig;
import com.xuecheng.learning.service.CourseLearningService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @Classname ChooseCourseTask
 * @Description TODO
 * @Date 2021/9/5 17:34
 * @Author by YeLiu
 */
@Component
public class ChooseCourseTask {
    @Autowired
    CourseLearningService courseLearningService;
    @Autowired
    RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = RabbitMQConfig.XC_LEARNING_ADDCHOOSECOURSE)
    public void receiveChoosecourseTask(XcTask xcTask) throws ParseException {
        //解析消息
        String requestBody = xcTask.getRequestBody();
        Map map = JSON.parseObject(requestBody, Map.class);
        //取出消息内容
        String userId = (String) map.get("userId");
        String courseId = (String) map.get("courseId");
        String valid = (String) map.get("valid");
        Date startTime=null;
        Date endTime=null;
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY-MM-dd");
        if(StringUtils.isNotEmpty((String)map.get("startTime"))){
            startTime =simpleDateFormat.parse((String) map.get("startTime")) ;
        }
        if(StringUtils.isNotEmpty((String)map.get("endTime"))){
            endTime =simpleDateFormat.parse((String) map.get("endTime")) ;
        }
        //调用添加方法
        ResponseResult responseResult = courseLearningService.addCourse(userId, courseId, valid, startTime, endTime, xcTask);
        if(responseResult.isSuccess()){
            //选课成功，发送消息完成选课
            rabbitTemplate.convertAndSend(RabbitMQConfig.EX_LEARNING_ADDCHOOSECOURSE,RabbitMQConfig.XC_LEARNING_FINISHADDCHOOSECOURSE_KEY,xcTask);
        }

    }
}
