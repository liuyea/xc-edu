package com.xuecheng.learning.dao;

import com.xuecheng.framework.domain.task.XcTaskHis;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Classname XcTaskHisRepository
 * @Description TODO
 * @Date 2021/9/5 17:01
 * @Author by YeLiu
 */
public interface XcTaskHisRepository extends JpaRepository<XcTaskHis,String> {
}
