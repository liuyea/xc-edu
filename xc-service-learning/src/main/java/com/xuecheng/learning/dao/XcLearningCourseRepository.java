package com.xuecheng.learning.dao;

import com.xuecheng.framework.domain.learning.XcLearningCourse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Classname XcLearningCourseRepository
 * @Description TODO
 * @Date 2021/9/5 17:00
 * @Author by YeLiu
 */
public interface XcLearningCourseRepository extends JpaRepository<XcLearningCourse,String> {
    XcLearningCourse findByUserIdAndCourseId(String userId,String courseId);
}
