package com.xuecheng.learning.service;

import com.xuecheng.framework.domain.course.TeachplanMediaPub;
import com.xuecheng.framework.domain.learning.XcLearningCourse;
import com.xuecheng.framework.domain.learning.response.GetMediaResult;
import com.xuecheng.framework.domain.learning.response.LearnCode;
import com.xuecheng.framework.domain.task.XcTask;
import com.xuecheng.framework.domain.task.XcTaskHis;
import com.xuecheng.framework.exception.ExceptionCast;
import com.xuecheng.framework.model.response.CommonCode;
import com.xuecheng.framework.model.response.ResponseResult;
import com.xuecheng.learning.client.CourseSearchClient;
import com.xuecheng.learning.dao.XcLearningCourseRepository;
import com.xuecheng.learning.dao.XcTaskHisRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

/**
 * @Classname CourseLearningService
 * @Description TODO
 * @Date 2021/8/19 15:46
 * @Author by YeLiu
 */
@Service
public class CourseLearningService {
        @Autowired
        CourseSearchClient courseSearchClient;
        @Autowired
    XcLearningCourseRepository xcLearningCourseRepository;
        @Autowired
    XcTaskHisRepository xcTaskHisRepository;

    public GetMediaResult getmedia(String courseId, String teachplanId) {
        //远程调用搜索服务查询媒资信息
        TeachplanMediaPub teachplanMediaPub = courseSearchClient.getmedia(teachplanId);
        if(teachplanMediaPub ==null || StringUtils.isEmpty(teachplanMediaPub.getMediaUrl())){
            ExceptionCast.cast(LearnCode.LEARNING_GETMEDIA_ERROR);
        }
        return new GetMediaResult(CommonCode.SUCCESS,teachplanMediaPub.getMediaUrl());
    }
    @Transactional
    public ResponseResult addCourse(String userId, String courseId, String valid, Date startTime, Date endTime, XcTask xcTask){
        if(StringUtils.isEmpty(courseId)){
            ExceptionCast.cast(LearnCode.LEARNING_GETMEDIA_ERROR);
        }
        if(StringUtils.isEmpty(userId)){
            ExceptionCast.cast(LearnCode.CHOOSECOURSE_USERISNULL);
        }
        if(StringUtils.isEmpty(xcTask.getId())){
            ExceptionCast.cast(LearnCode.CHOOSECOURSE_TASKISNULL);
        }
        XcLearningCourse xcLearningCourse = xcLearningCourseRepository.findByUserIdAndCourseId(userId, courseId);
        if(xcLearningCourse!=null){
            //不为空则更新
            xcLearningCourse.setStartTime(startTime);
            xcLearningCourse.setEndTime(endTime);
            xcLearningCourse.setStatus("501001");
            xcLearningCourse.setValid(valid);
            xcLearningCourseRepository.save(xcLearningCourse);
        }else{
            //为空则添加
            xcLearningCourse=new XcLearningCourse();
            xcLearningCourse.setUserId(userId);
            xcLearningCourse.setCourseId(courseId);
            xcLearningCourse.setStartTime(startTime);
            xcLearningCourse.setEndTime(endTime);
            xcLearningCourse.setStatus("501001");
            xcLearningCourse.setValid(valid);
            xcLearningCourseRepository.save(xcLearningCourse);
        }
        Optional<XcTaskHis> byId = xcTaskHisRepository.findById(xcTask.getId());
        if(!byId.isPresent()){
            XcTaskHis xcTaskHis=new XcTaskHis();
            BeanUtils.copyProperties(xcTask,xcTaskHis);
            xcTaskHisRepository.save(xcTaskHis);
        }
        return  new ResponseResult(CommonCode.SUCCESS);
    }
}
