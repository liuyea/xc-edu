package com.xuecheng.order.service;


import com.xuecheng.framework.domain.task.XcTask;
import com.xuecheng.framework.domain.task.XcTaskHis;
import com.xuecheng.order.dao.XcTaskHisRepository;
import com.xuecheng.order.dao.XcTaskRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Classname XcTaskService
 * @Description TODO
 * @Date 2021/9/3 15:31
 * @Author by YeLiu
 */
@Service
public class XcTaskService {
    @Autowired
    XcTaskRepository xcTaskRepository;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    XcTaskHisRepository xcTaskHisRepository;

    public List<XcTask> findByUpdateTimeBefore(Date updateTime, int size){
        Pageable pageable=new PageRequest(0,size);
        Page<XcTask> list = xcTaskRepository.findByUpdateTimeBefore(pageable, updateTime);
        List<XcTask> content = list.getContent();
        return  content;

    }
    //发布消息 将添加选课方法发送给mq
    public void publish(XcTask xcTask,String ex,String rountingKey){
        Optional<XcTask> op = xcTaskRepository.findById(xcTask.getId());
        if(op.isPresent()){
            XcTask xcTask1 = op.get();
            rabbitTemplate.convertAndSend(ex,rountingKey,xcTask);
            xcTask1.setUpdateTime(new Date());
            xcTaskRepository.save(xcTask1);
        }

    }
    @Transactional
    public int getTask(String id,int version){
        //通过乐观锁的方式来更新数据表，如果结果大于1说明可以取到任务
        int i = xcTaskRepository.updateVersionById(id, version);
        return i;
    }

    public void finishTask(String taskId){
        Optional<XcTask> byId = xcTaskRepository.findById(taskId);
        if(byId.isPresent()){
            //当前任务
            XcTask xcTask = byId.get();
            //历史任务
            XcTaskHis xcTaskHis=new XcTaskHis();
            BeanUtils.copyProperties(xcTask, xcTaskHis);
            xcTaskHisRepository.save(xcTaskHis);
            xcTaskRepository.delete(xcTask);
        }
    }

}
