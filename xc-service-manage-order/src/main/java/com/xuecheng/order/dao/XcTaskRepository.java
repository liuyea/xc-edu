package com.xuecheng.order.dao;


import com.xuecheng.framework.domain.task.XcTask;
import feign.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * @Classname XcTaskRepository
 * @Description TODO
 * @Date 2021/9/3 15:29
 * @Author by YeLiu
 */
public interface XcTaskRepository extends JpaRepository<XcTask,String> {
    //查询某个时间之前的前n条记录
    Page<XcTask> findByUpdateTimeBefore(Pageable pageable, Date updateTime);
    //根据id更新时间
    @Modifying
    @Query(value = "update XcTask t set t.updateTime=:updateTime where t.id=:id")
    public int updateTimeById(@Param("id")String id, @Param("updateTime") Date updateTime);
    @Modifying
    @Query(value = "update XcTask t set t.version=:version+1 where t.id=:id and t.version=:version")
    public int updateVersionById(@Param("id")String id, @Param("version") int version);
}
