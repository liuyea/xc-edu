package com.xuecheng.order.dao;


import com.xuecheng.framework.domain.task.XcTask;
import com.xuecheng.framework.domain.task.XcTaskHis;
import feign.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * @Classname XcTaskRepository
 * @Description TODO
 * @Date 2021/9/3 15:29
 * @Author by YeLiu
 */
public interface XcTaskHisRepository extends JpaRepository<XcTaskHis,String> {

}
