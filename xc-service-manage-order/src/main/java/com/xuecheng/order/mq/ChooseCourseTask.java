package com.xuecheng.order.mq;

import com.xuecheng.framework.domain.task.XcTask;
import com.xuecheng.order.config.RabbitMQConfig;
import com.xuecheng.order.service.XcTaskService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @Classname ChooseCourseTask
 * @Description TODO
 * @Date 2021/9/1 15:51
 * @Author by YeLiu
 */
@Component
public class ChooseCourseTask {
    private static final Logger logger= LoggerFactory.getLogger(ChooseCourseTask.class);
    @Autowired
    XcTaskService xcTaskService;

    @RabbitListener(queues = RabbitMQConfig.XC_LEARNING_FINISHADDCHOOSECOURSE)
    public void finish(XcTask xcTask){
        if(xcTask!=null && StringUtils.isNotEmpty(xcTask.getId())){
            xcTaskService.finishTask(xcTask.getId());
        }
    }
    @Scheduled(fixedRate =3000) //上次执行开始时间后5秒执行
    public void sendChoosecourseTask(){
        Calendar calendar=new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(GregorianCalendar.MINUTE,-1);
        Date time = calendar.getTime();
        List<XcTask> xcTaskList = xcTaskService.findByUpdateTimeBefore(time, 100);
        System.out.println(xcTaskList);
        //调用service发布消息
            for( XcTask xc:xcTaskList){//取到任务，判断
                if(xcTaskService.getTask(xc.getId(),xc.getVersion())>0){
                    String mqExchange = xc.getMqExchange();
                    String mqRoutingkey = xc.getMqRoutingkey();
                    xcTaskService.publish(xc,mqExchange,mqRoutingkey);
                }

            }
    }
   // @Scheduled(fixedRate =3000) //上次执行开始时间后5秒执行
    public void task1(){
        logger.info("===============测试定时任务1开始===============");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("===============测试定时任务1结束===============");
    }

//    @Scheduled(fixedRate =3000) //上次执行开始时间后5秒执行
    public void task2(){
        logger.info("===============测试定时任务2开始===============");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("===============测试定时任务2结束===============");
    }
}
