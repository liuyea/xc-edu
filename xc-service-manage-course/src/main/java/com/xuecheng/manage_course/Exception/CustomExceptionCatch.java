package com.xuecheng.manage_course.Exception;

import com.xuecheng.framework.exception.ExceptionCatch;
import com.xuecheng.framework.model.response.CommonCode;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @Classname CustomExceptionCatch
 * @Description TODO
 * @Date 2021/8/26 15:56
 * @Author by YeLiu
 */
@ControllerAdvice
public class CustomExceptionCatch extends ExceptionCatch {
    static {
        builder.put(AccessDeniedException.class,CommonCode.UNAUTHORISE);
    }
}
