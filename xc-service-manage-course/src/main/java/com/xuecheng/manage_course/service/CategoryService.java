package com.xuecheng.manage_course.service;

import com.xuecheng.framework.domain.course.ext.CategoryNode;
import com.xuecheng.manage_course.dao.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Classname CategoryService
 * @Description TODO
 * @Date 2021/8/4 16:10
 * @Author by YeLiu
 */
@Service
public class CategoryService {
    @Autowired
    CategoryMapper categoryMapper;
    public CategoryNode find(){
        return categoryMapper.find();
    }
}
