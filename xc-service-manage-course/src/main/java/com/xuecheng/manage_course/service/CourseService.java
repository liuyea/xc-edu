package com.xuecheng.manage_course.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.response.CmsPageResult;
import com.xuecheng.framework.domain.cms.response.CmsPostPageResult;
import com.xuecheng.framework.domain.course.*;
import com.xuecheng.framework.domain.course.ext.CourseInfo;
import com.xuecheng.framework.domain.course.ext.CourseView;
import com.xuecheng.framework.domain.course.ext.TeachplanNode;
import com.xuecheng.framework.domain.course.request.CourseListRequest;
import com.xuecheng.framework.domain.course.response.AddCourseResult;
import com.xuecheng.framework.domain.course.response.CourseCode;
import com.xuecheng.framework.domain.course.response.CoursePublishResult;
import com.xuecheng.framework.exception.ExceptionCast;
import com.xuecheng.framework.model.response.CommonCode;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.QueryResult;
import com.xuecheng.framework.model.response.ResponseResult;
import com.xuecheng.manage_course.client.CmsPageClient;
import com.xuecheng.manage_course.dao.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.spring.web.json.Json;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Classname CourseService
 * @Description TODO
 * @Date 2021/8/3 15:09
 * @Author by YeLiu
 */
@Service
public class CourseService {
    @Autowired
    TeachplanMapper teachplanMapper;
    @Autowired
    TeachplanRepository teachplanRepository;
    @Autowired
    CourseBaseRepository courseBaseRepository;
    @Autowired
    CourseMapper courseMapper;
    @Autowired
    CourseMarketRepository courseMarketRepository;
    @Autowired
    CoursePicRepository coursePicRepository;
    @Autowired
    CmsPageClient cmsPageClient;
    @Autowired
    CoursePubRepository coursePubRepository;
    @Autowired
    TeachplanMediaRepository teachplanMediaRepository;
    @Autowired
    TeachplanMediaPubRepository teachplanMediaPubRepository;

    @Value("${course‐publish.dataUrlPre}")
    private String publish_dataUrlPre;
    @Value("${course‐publish.pagePhysicalPath}")
    private String publish_page_physicalpath;
    @Value("${course‐publish.pageWebPath}")
    private String publish_page_webpath;
    @Value("${course‐publish.siteId}")
    private String publish_siteId;
    @Value("${course‐publish.templateId}")
    private String publish_templateId;
    @Value("${course‐publish.previewUrl}")
    private String previewUrl;

    //课程计划查询
    public TeachplanNode findTeachplan(String courseId) {
        return teachplanMapper.selectList(courseId);
    }

    //课程计划添加
    @Transactional
    public ResponseResult addTeachplan(Teachplan teachplan) {
        if (teachplan == null || StringUtils.isEmpty(teachplan.getCourseid()) ||
                StringUtils.isEmpty(teachplan.getPname())) {
            ExceptionCast.cast(CommonCode.FAIL);
        }
        String courseid = teachplan.getCourseid();
        //页面父id
        String parentid = teachplan.getParentid();
        if (StringUtils.isEmpty(parentid)) {
            //父节点为空执行
            parentid = getTeachplanRoot(courseid);
        }
        Teachplan teachplanNew = new Teachplan();
        //复制页面数据
        BeanUtils.copyProperties(teachplan, teachplanNew);
        teachplanNew.setCourseid(courseid);
        teachplanNew.setParentid(parentid);
        //设置级别 根据父节点级别判断
        Optional<Teachplan> optionalTeachplan = teachplanRepository.findById(parentid);
        if (!optionalTeachplan.isPresent()) {
            ExceptionCast.cast(CommonCode.FAIL);
        }
        Teachplan teachplan1 = optionalTeachplan.get();
        String ParentGrade = teachplan1.getGrade();
        if (ParentGrade.equals("1")) {
            teachplanNew.setGrade("2");
        } else {
            teachplanNew.setGrade("3");
        }
        teachplanRepository.save(teachplanNew);
        return new ResponseResult(CommonCode.SUCCESS);
    }
//查询课程的根节点，查询不到自动添加根节点

    private String getTeachplanRoot(String courseId) {

        Optional<CourseBase> op = courseBaseRepository.findById(courseId);
        if (!op.isPresent()) {
            return null;
        }
        CourseBase courseBase = op.get();
        List<Teachplan> list = teachplanRepository.findByCourseidAndParentid(courseId, "0");
        if (list == null || list.size() <= 0) {
            Teachplan teachplan = new Teachplan();
            teachplan.setCourseid(courseId);
            teachplan.setPname(courseBase.getName());
            teachplan.setParentid("0");
            teachplan.setGrade("1");//1级
            teachplan.setStatus("0");//未发布
            teachplanRepository.save(teachplan);
        }
        Teachplan teachplan = list.get(0);
        return teachplan.getId();
    }

    //查询我的课程列表
    public QueryResponseResult<CourseInfo> findCourseList(String companyId,int page, int size, CourseListRequest courseListRequest) {
        if (courseListRequest == null) {
            courseListRequest = new CourseListRequest();
        }
        courseListRequest.setCompanyId(companyId);
        if (page <= 0) {
            page = 0;
        }
        if (size <= 0) {
            size = 20;
        }

        PageHelper.startPage(page, size);
        Page<CourseInfo> courseList = courseMapper.findCourseList(courseListRequest);
        List<CourseInfo> result = courseList.getResult();
        long total = courseList.getTotal();
        QueryResult<CourseInfo> courseInfoQueryResult = new QueryResult<>();
        courseInfoQueryResult.setTotal(total);
        courseInfoQueryResult.setList(result);
        return new QueryResponseResult<CourseInfo>(CommonCode.SUCCESS, courseInfoQueryResult);
    }

    //添加课程基本信息
    @Transactional
    public AddCourseResult addCourseBase(CourseBase courseBase) {
        courseBase.setStatus("202001");
        courseBaseRepository.save(courseBase);
        System.out.println(courseBase.getId());
        return new AddCourseResult(CommonCode.SUCCESS, courseBase.getId());
    }

    //查询课程基本信息
    public CourseBase findCourseById(String courseId) {
        Optional<CourseBase> byId = courseBaseRepository.findById(courseId);
        if (!byId.isPresent()) {
            return null;
        }
        CourseBase courseBase = byId.get();
        return courseBase;
    }

    //修改课程基本信息
    @Transactional
    public ResponseResult updateCoursebase(String id, CourseBase courseBase) {
        CourseBase one = this.findCourseById(id);
        if (one == null) {
            ExceptionCast.cast(CommonCode.FAIL);
        }
        one.setName(courseBase.getName());
        one.setMt(courseBase.getMt());
        one.setSt(courseBase.getSt());
        one.setGrade(courseBase.getGrade());
        one.setStudymodel(courseBase.getStudymodel());
        one.setUsers(courseBase.getUsers());
        one.setDescription(courseBase.getDescription());
        CourseBase save = courseBaseRepository.save(one);
        return new ResponseResult(CommonCode.SUCCESS);
    }

    //获取课程营销信息
    public CourseMarket getCourseMarketById(String courseId) {
        Optional<CourseMarket> byId = courseMarketRepository.findById(courseId);
        if (byId.isPresent()) {
            CourseMarket courseMarket = byId.get();
            return courseMarket;
        }
        return null;
    }

    //修改或者添加课程营销信息
    @Transactional
    public CourseMarket updateCourseMarket(String id, CourseMarket courseMarket) {
        CourseMarket one = this.getCourseMarketById(id);
        if (one != null) {
            //不为空，修改营销信息
            one.setCharge(courseMarket.getCharge());
            one.setStartTime(courseMarket.getStartTime());//课程有效期，开始时间
            one.setEndTime(courseMarket.getEndTime());//课程有效期，结束时间
            one.setPrice(courseMarket.getPrice());
            one.setQq(courseMarket.getQq());
            one.setValid(courseMarket.getValid());
            courseMarketRepository.save(one);
        } else {
            //为空，添加营销信息
            one = new CourseMarket();
            BeanUtils.copyProperties(courseMarket, one);
            courseMarketRepository.save(one);
        }
        return one;
    }

    //查询课程试图，包括课程详情，营销，图片，课程计划
    public CourseView getCoruseView(String id) {
        CourseView courseView = new CourseView();
        //查询课程基本信息
        Optional<CourseBase> optional = courseBaseRepository.findById(id);
        if (optional.isPresent()) {
            CourseBase courseBase = optional.get();
            courseView.setCourseBase(courseBase);
        }
        //查询课程营销信息
        Optional<CourseMarket> courseMarketOptional = courseMarketRepository.findById(id);
        if (courseMarketOptional.isPresent()) {
            CourseMarket courseMarket = courseMarketOptional.get();
            courseView.setCourseMarket(courseMarket);
        }
        //查询课程图片信息
        Optional<CoursePic> picOptional = coursePicRepository.findById(id);
        if (picOptional.isPresent()) {
            CoursePic coursePic = picOptional.get();
            courseView.setCoursePic(coursePic);
        }
        //查询课程计划信息
        TeachplanNode teachplanNode = teachplanMapper.selectList(id);
        courseView.setTeachplanNode(teachplanNode);
        return courseView;
    }

    //根据id查询课程基本信息

    public CourseBase findCourseBaseById(String courseId) {
        Optional<CourseBase> baseOptional = courseBaseRepository.findById(courseId);
        if (baseOptional.isPresent()) {

            CourseBase courseBase = baseOptional.get();
            return courseBase;
        }
        ExceptionCast.cast(CourseCode.COURSE_GET_NOTEXISTS);
        return null;
    }

    /*
     * @Description 课程预览
     * @param id
     * @return com.xuecheng.framework.domain.course.response.CoursePublishResult
     * @date 2021/8/8 13:55
     * @auther YeLiu
     */
    public CoursePublishResult preview(String id) {
        CourseBase courseBase = this.findCourseBaseById(id);
        //请求cms添加页面
        //准备CmsPage
        CmsPage cmsPage = new CmsPage();
        cmsPage.setSiteId(publish_siteId);
        cmsPage.setTemplateId(publish_templateId);
        cmsPage.setPageWebPath(publish_page_webpath);
        cmsPage.setPagePhysicalPath(publish_page_physicalpath);
        cmsPage.setDataUrl(publish_dataUrlPre+id);
        cmsPage.setPageAliase(courseBase.getName());
        cmsPage.setPageName(id+".html");
        //远程调用save方法
        CmsPageResult responseResult = cmsPageClient.saveCmsPage(cmsPage);
        if (responseResult == null) {
            //抛出异常
            return new CoursePublishResult(CommonCode.FAIL,null);
        }
        CmsPage cmsPage1 = responseResult.getCmsPage();
        String pageId = cmsPage1.getPageId();

        //拼装页面预览的url
        String url = previewUrl + pageId;
        //返回CoursePublishResult（其中包括课程预览的url）
        return new CoursePublishResult(CommonCode.SUCCESS,url);
    }
    //课程发布
    @Transactional
    public CoursePublishResult publish(String id) {
        //查询课程
        CourseBase courseBase = this.findCourseBaseById(id);
        //请求cms添加页面
        //准备CmsPage
        CmsPage cmsPage = new CmsPage();
        cmsPage.setSiteId(publish_siteId);
        cmsPage.setTemplateId(publish_templateId);
        cmsPage.setPageWebPath(publish_page_webpath);
        cmsPage.setPagePhysicalPath(publish_page_physicalpath);
        cmsPage.setDataUrl(publish_dataUrlPre+id);
        cmsPage.setPageAliase(courseBase.getName());
        cmsPage.setPageName(id+".html");
        //调用cms一键发布接口将课程详情发布到浏览器
        CmsPostPageResult cmsPostPageResult = cmsPageClient.postPageQuick(cmsPage);
        if(!cmsPostPageResult.isSuccess()){
            ExceptionCast.cast(CommonCode.FAIL);
        }
        //发布完成后更改发布状态为“已发布”
        CourseBase courseBase1 = this.saveCoursePubState(id);
        if(courseBase1==null){
            ExceptionCast.cast(CommonCode.FAIL);
        }
        //保存课程索引信息
        //创建CoursePub对象
        CoursePub coursePub = this.createCoursePub(id);
        //将coursePub对象保存到数据库
        this.saveCoursePub(id, coursePub);

        //缓存课程信息
        //得到页面url
        String pageUrl = cmsPostPageResult.getPageUrl();
        //发布完成以后向媒资管理里插入信息
        saveTeachplanMediaPub(id);
        return new CoursePublishResult(CommonCode.SUCCESS, pageUrl) ;
    }
    //向teachplanmediaPub中保存信息
    private void saveTeachplanMediaPub(String courseId){
        //先删除TeachplanMediaPub中的记录
        teachplanMediaPubRepository.deleteByCourseId(courseId);
        //查询teachplanMedia中的列表信息
        List<TeachplanMedia> teachplanMediaList = teachplanMediaRepository.findByCourseId(courseId);
        List<TeachplanMediaPub> teachplanMediaPubs=new ArrayList<>();
        //将TeachplanMedia的列表信息保存到TeachplanMediaPub中
        for(TeachplanMedia t:teachplanMediaList){
            TeachplanMediaPub teachplanMediaPub=new TeachplanMediaPub();
            BeanUtils.copyProperties(t, teachplanMediaPub);
            teachplanMediaPub.setTimestamp(new Date());
            teachplanMediaPubs.add(teachplanMediaPub);
        }
        teachplanMediaPubRepository.saveAll(teachplanMediaPubs);


    }
    //将coursePub对象保存到数据库
    private CoursePub saveCoursePub(String id,CoursePub coursePub){
        Optional<CoursePub> coursePubOptional = coursePubRepository.findById(id);
        CoursePub coursePubNew=null;
        if(coursePubOptional.isPresent()){
            coursePubNew=coursePubOptional.get();
        }else {
            coursePubNew=new CoursePub();
        }
        BeanUtils.copyProperties(coursePub, coursePubNew);
        coursePub.setId(id);
        //时间戳 给logstash使用
        coursePub.setTimestamp(new Date());
        //发布时间
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        coursePub.setPubTime(format);
        CoursePub save = coursePubRepository.save(coursePub);
        return save;
    }
    //创建coursePub对象
    private CoursePub createCoursePub(String id){
        CoursePub coursePub=new CoursePub();
        //根据课程id查询course_base
        Optional<CourseBase> baseOptional = courseBaseRepository.findById(id);
        if(baseOptional.isPresent()){
            CourseBase courseBase = baseOptional.get();
            //将courseBase属性拷贝到CoursePub中
            BeanUtils.copyProperties(courseBase,coursePub);
        }

        //查询课程图片
        Optional<CoursePic> picOptional = coursePicRepository.findById(id);
        if(picOptional.isPresent()){
            CoursePic coursePic = picOptional.get();
            BeanUtils.copyProperties(coursePic, coursePub);
        }

        //课程营销信息
        Optional<CourseMarket> marketOptional = courseMarketRepository.findById(id);
        if(marketOptional.isPresent()){
            CourseMarket courseMarket = marketOptional.get();
            BeanUtils.copyProperties(courseMarket, coursePub);
        }
        //课程计划信息
        TeachplanNode teachplanNode = teachplanMapper.selectList(id);
        String jsonString = JSON.toJSONString(teachplanNode);
        //将课程计划信息json串保存到coursePub中
        coursePub.setTeachplan(jsonString);

        return coursePub;
    }
    //更改课程状态为202002
    private CourseBase saveCoursePubState(String id){
        CourseBase courseBaseById = this.findCourseBaseById(id);
        courseBaseById.setStatus("202002");
        CourseBase save = courseBaseRepository.save(courseBaseById);
        return save;
    }

    public ResponseResult savemedia(TeachplanMedia teachplanMedia) {
        if(teachplanMedia==null || StringUtils.isEmpty(teachplanMedia.getTeachplanId())){
        ExceptionCast.cast(CommonCode.FAIL);
        }
        //查询课程计划 判断等级是不是三级
        String teachplanId = teachplanMedia.getTeachplanId();
        Optional<Teachplan> op = teachplanRepository.findById(teachplanId);
        if(!op.isPresent()){
            ExceptionCast.cast(CommonCode.FAIL);
        }
        Teachplan teachplan = op.get();
        String grade = teachplan.getGrade();
        if(grade==null || !grade.equals("3")){
            ExceptionCast.cast(CourseCode.COURSE_MEDIA_TEACHPLAN_GRADEERROR);
        }
        //查询 teachplanMedia、看是否绑定了视频
        Optional<TeachplanMedia> byId = teachplanMediaRepository.findById(teachplanId);
        TeachplanMedia one=null;
        if(byId.isPresent()){
            //有值，已经被绑定，修改原有信息
           one= byId.get();
        }else{
            //没有值，new 一个空的
            one=new TeachplanMedia();
        }
        //保存one到数据库中
        one.setCourseId(teachplanMedia.getCourseId());
        one.setMediaFileOriginalName(teachplanMedia.getMediaFileOriginalName());
        one.setMediaId(teachplanMedia.getMediaId());
        one.setMediaUrl(teachplanMedia.getMediaUrl());
        one.setTeachplanId(teachplanId);
        teachplanMediaRepository.save(one);
        return new ResponseResult(CommonCode.SUCCESS);
    }
}
