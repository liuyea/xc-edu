package com.xuecheng.manage_course.dao;

import com.xuecheng.framework.domain.course.CourseBase;
import com.xuecheng.framework.domain.course.ext.TeachplanNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator.
 * @author liu ye
 */
@Mapper
public interface TeachplanMapper {
   public TeachplanNode selectList(String courseId);
}
