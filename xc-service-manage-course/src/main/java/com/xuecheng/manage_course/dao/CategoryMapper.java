package com.xuecheng.manage_course.dao;

import com.xuecheng.framework.domain.course.ext.CategoryNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Classname CategoryMapper
 * @Description TODO
 * @Date 2021/8/4 16:04
 * @Author by YeLiu
 */
@Mapper
public interface CategoryMapper {
    public CategoryNode find();
}
