package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Classname CmsPageRepository
 * @Description TODO
 * @Date 2021/7/17 11:19
 * @Author by YeLiu
 */
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {
    //根据页面名称，站点id，页面路径查询
    CmsPage findByPageNameAndSiteIdAndPageWebPath(String pageName,String siteId,String pageWebPath );

}
