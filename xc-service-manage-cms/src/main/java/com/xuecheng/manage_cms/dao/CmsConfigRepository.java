package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsConfig;
import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Classname CmsPageRepository
 * @Description TODO
 * @Date 2021/7/17 11:19
 * @Author by YeLiu
 */
public interface CmsConfigRepository extends MongoRepository<CmsConfig,String> {

}
