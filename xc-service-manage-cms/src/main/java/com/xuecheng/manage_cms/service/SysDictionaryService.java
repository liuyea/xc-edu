package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.system.SysDictionary;
import com.xuecheng.manage_cms.dao.SysDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Classname SysDictionaryService
 * @Description TODO
 * @Date 2021/8/4 16:38
 * @Author by YeLiu
 */
@Service
public class SysDictionaryService {
    @Autowired
    SysDictionaryRepository sysDictionaryRepository;
    public SysDictionary findByType(String Type){
       return sysDictionaryRepository.findByDType(Type);
    }
}
