package com.xuecheng.manage_cms.controller;

import com.xuecheng.api.cms.CmsConfigControllerApi;
import com.xuecheng.framework.domain.cms.CmsConfig;
import com.xuecheng.framework.web.BaseController;
import com.xuecheng.manage_cms.service.PageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

/**
 * @Classname CmsConfigController
 * @Description TODO
 * @Date 2021/7/26 14:41
 * @Author by YeLiu
 */
@Controller
public class CmsPagePreviewController extends BaseController {
    @Autowired
    PageService pageService;

    @RequestMapping(value = "/cms/preview/{pageId}", method = RequestMethod.GET)
    public void preview(@PathVariable("pageId") String pageId) {
        String pageHtml = pageService.getPageHtml(pageId);
        if (StringUtils.isNotEmpty(pageHtml)) {
            try {
                ServletOutputStream outputStream = response.getOutputStream();
               response.setHeader("Context-type", "text/html;charset=utf-8");
                outputStream.write(pageHtml.getBytes("utf-8"));
            } catch ( IOException e) {
                e.printStackTrace();
            }
        }
    }

}
