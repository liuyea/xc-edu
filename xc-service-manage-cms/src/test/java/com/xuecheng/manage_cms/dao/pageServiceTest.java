package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.manage_cms.service.PageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;


/**
 * @Classname CmsPageRepositoryTest
 * @Description TODO
 * @Date 2021/7/17 11:25
 * @Author by YeLiu
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class pageServiceTest {
    @Autowired
    PageService pageService;

    @Test
    public void testgetHtml() {
        String pageHtml = pageService.getPageHtml("60f93967da4a666ce4c5347b");
        System.out.println(pageHtml);
    }



}
