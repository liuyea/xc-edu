package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;


/**
 * @Classname CmsPageRepositoryTest
 * @Description TODO
 * @Date 2021/7/17 11:25
 * @Author by YeLiu
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CmsPageRepositoryTest {
    @Autowired
    CmsPageRepository cmsPageRepository;

    @Test
    public void testCmsPageRepository() {
        List<CmsPage> all = cmsPageRepository.findAll();
        System.out.println(all);
    }

    @Test
    public void testFindPage() {
        int page = 0;
        int size = 10;
        Pageable pageabl = PageRequest.of(page, size);
        Page<CmsPage> all = cmsPageRepository.findAll(pageabl);
        System.out.println(all);
    }
    @Test
    public void testFindPageAndExample() {
        int page = 0;
        int size = 10;
        Pageable pageabl = PageRequest.of(page, size);
        CmsPage cmsPage=new CmsPage();
        cmsPage.setPageAliase("课程");
        ExampleMatcher exampleMatcher=ExampleMatcher.matching().
       withMatcher("pageAliase", ExampleMatcher.GenericPropertyMatchers.contains());
        Example<CmsPage> example=Example.of(cmsPage,exampleMatcher);
        Page<CmsPage> all = cmsPageRepository.findAll(example,pageabl);
        System.out.println(all);
    }

    //修改
    @Test
    public void testUpdate() {
        Optional<CmsPage> optional =cmsPageRepository.findById("5b17a34211fe5e2ee8c116c9");
        if (optional.isPresent()) {
            CmsPage cmsPage = optional.get();
            cmsPage.setPageAliase("test01");
            cmsPageRepository.save(cmsPage);
        }
    }
}
