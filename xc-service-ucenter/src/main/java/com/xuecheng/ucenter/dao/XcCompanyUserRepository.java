package com.xuecheng.ucenter.dao;

import com.xuecheng.framework.domain.ucenter.XcCompanyUser;
import com.xuecheng.framework.domain.ucenter.XcUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Classname XcUserRepository
 * @Description TODO
 * @Date 2021/8/23 10:31
 * @Author by YeLiu
 */
public interface XcCompanyUserRepository extends JpaRepository<XcCompanyUser,String> {
    //根据用户id查询用户公司信息
    public XcCompanyUser findByUserId(String userId);


}
