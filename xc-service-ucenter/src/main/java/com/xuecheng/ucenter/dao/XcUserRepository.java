package com.xuecheng.ucenter.dao;

import com.xuecheng.framework.domain.ucenter.XcUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Classname XcUserRepository
 * @Description TODO
 * @Date 2021/8/23 10:31
 * @Author by YeLiu
 */
public interface XcUserRepository extends JpaRepository<XcUser,String> {

    public XcUser findByUsername(String username);
}
