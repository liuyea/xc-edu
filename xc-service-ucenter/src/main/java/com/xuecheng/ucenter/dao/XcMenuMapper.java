package com.xuecheng.ucenter.dao;

import com.xuecheng.framework.domain.ucenter.XcMenu;
import com.xuecheng.framework.domain.ucenter.ext.XcMenuExt;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Classname XCMenuMapper
 * @Description TODO
 * @Date 2021/8/27 15:43
 * @Author by YeLiu
 */
@Mapper
public interface XcMenuMapper {
    //根据用户id查询用户权限
    public List<XcMenu> selectPermissionByUserId(String userid);
}
