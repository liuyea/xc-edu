package com.xuecheng.ucenter.service;

import com.xuecheng.framework.domain.ucenter.XcCompanyUser;
import com.xuecheng.framework.domain.ucenter.XcMenu;
import com.xuecheng.framework.domain.ucenter.XcUser;
import com.xuecheng.framework.domain.ucenter.ext.XcUserExt;
import com.xuecheng.ucenter.dao.XcCompanyUserRepository;
import com.xuecheng.ucenter.dao.XcMenuMapper;
import com.xuecheng.ucenter.dao.XcUserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Classname UserService
 * @Description TODO
 * @Date 2021/8/23 10:34
 * @Author by YeLiu
 */
@Service
public class UserService {
    @Autowired
    XcUserRepository xcUserRepository;
    @Autowired
    XcCompanyUserRepository xcCompanyUserRepository;
    @Autowired
    XcMenuMapper xcMenuMapper;
    //根据用户账号查询用户xcUser信息
    public XcUser findXcUserByUsername(String username){
      return  xcUserRepository.findByUsername(username);
    }

    public XcUserExt getUserExt(String username) {
    //根据用户账号查询用户xcUser信息
        XcUser xcUser= this.findXcUserByUsername(username);
        if(xcUser==null){
            return null;
        }
        String id = xcUser.getId();
        List<XcMenu> xcMenus = xcMenuMapper.selectPermissionByUserId(id);
        XcCompanyUser xcCompanyUser = xcCompanyUserRepository.findByUserId(id);
        String companyId=null;
        if(xcCompanyUser!=null){
            companyId=xcCompanyUser.getCompanyId();
        }
        XcUserExt xcUserExt=new XcUserExt();
        BeanUtils.copyProperties(xcUser,xcUserExt);
        xcUserExt.setCompanyId(companyId);
        //设置权限
        xcUserExt.setPermissions(xcMenus);
        return xcUserExt;
    }
}
