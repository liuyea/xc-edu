package com.xuecheng.test.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.sql.Blob;
import java.util.concurrent.TimeoutException;

/**
 * @Classname Producter01
 * @Description TODO
 * @Date 2021/7/29 15:32
 * @Author by YeLiu
 */
public class Producter01 {
    private static final String QUEUE ="hello word" ;

    public static void main(String[] args) {
        ConnectionFactory connectionFactory=new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        Connection connection =null;
        Channel channel =null;
        try {
       connection = connectionFactory.newConnection();
           channel = connection.createChannel();
            channel.queueDeclare(QUEUE,true,false,false,null);
            String message="helloworld小明\"+System.currentTimeMillis()";
            channel.basicPublish("", QUEUE, null, message.getBytes());
            System.out.println("Send Message is:'" + message + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                channel.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
