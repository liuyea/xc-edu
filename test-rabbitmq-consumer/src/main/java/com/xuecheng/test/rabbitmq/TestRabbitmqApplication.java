package com.xuecheng.test.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Classname TestRabbitmqApplication
 * @Description TODO
 * @Date 2021/7/30 14:58
 * @Author by YeLiu
 */
@SpringBootApplication
public class TestRabbitmqApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestRabbitmqApplication.class,args);
    }
}
