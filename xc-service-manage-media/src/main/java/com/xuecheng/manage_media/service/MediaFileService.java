package com.xuecheng.manage_media.service;


import com.xuecheng.framework.domain.media.MediaFile;
import com.xuecheng.framework.domain.media.request.QueryMediaFileRequest;
import com.xuecheng.framework.model.response.CommonCode;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.QueryResult;
import com.xuecheng.manage_media.dao.MediaFileRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;



import java.io.*;
import java.util.*;

/**
 * @Classname MediaUploadService
 * @Description TODO
 * @Date 2021/8/16 15:00
 * @Author by YeLiu
 */
@Service
public class MediaFileService {
    @Autowired
    MediaFileRepository mediaFileRepository;


    public QueryResponseResult findList(int page, int size, QueryMediaFileRequest queryMediaFileRequest) {
        if(queryMediaFileRequest==null){
            queryMediaFileRequest=new QueryMediaFileRequest();
        }
        ////查询条件
        MediaFile mediaFile=new MediaFile();
        if(StringUtils.isNotEmpty(queryMediaFileRequest.getTag())){
            mediaFile.setTag(queryMediaFileRequest.getTag());
        }
        if(StringUtils.isNotEmpty(queryMediaFileRequest.getFileOriginalName())){
            mediaFile.setFileOriginalName(queryMediaFileRequest.getFileOriginalName());
        }
        if(StringUtils.isNotEmpty(queryMediaFileRequest.getProcessStatus())){
            mediaFile.setProcessStatus(queryMediaFileRequest.getProcessStatus());
        }
        //查询匹配器
        ExampleMatcher matching = ExampleMatcher.matching()
                                    .withMatcher("tag", ExampleMatcher.GenericPropertyMatchers.contains())
                                    .withMatcher("fileOriginalName", ExampleMatcher.GenericPropertyMatchers.contains());
        //定义example实例
        Example<MediaFile> example=Example.of(mediaFile,matching);
        if(page<=1){
            page=1;
        }
        page=page-1;
        if(size<=1){
            size=5;
        }
        //分页查询
        Pageable pageable=new PageRequest(page,size);
        Page<MediaFile> all = mediaFileRepository.findAll(example, pageable);
        //总记录数
        long totalElements = all.getTotalElements();
        //数据
        List<MediaFile> content = all.getContent();
        QueryResult<MediaFile> queryResult=new QueryResult<>();
        queryResult.setList(content);
        queryResult.setTotal(totalElements);
        return new QueryResponseResult(CommonCode.SUCCESS,queryResult);
    }
}
