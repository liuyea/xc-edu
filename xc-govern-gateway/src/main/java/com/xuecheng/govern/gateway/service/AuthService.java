package com.xuecheng.govern.gateway.service;

import com.xuecheng.framework.domain.ucenter.ext.AuthToken;
import com.xuecheng.framework.utils.CookieUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Classname AuthService
 * @Description TODO
 * @Date 2021/8/25 15:38
 * @Author by YeLiu
 */
@Service
public class AuthService {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    //从header取出jwt令牌
    public String getToken(HttpServletRequest request){
        String authorization = request.getHeader("Authorization");
        if(StringUtils.isEmpty(authorization)){
            return null;
        }
        if(!authorization.startsWith("Bearer ")){
            return null;
        }
        String jwt=authorization.substring(7);
        return jwt;
    }

    //从cookie取出token
    public String getCookie(HttpServletRequest request){
        Map<String, String> map = CookieUtil.readCookie(request, "uid");
        String uid = map.get("uid");
        if(StringUtils.isEmpty(uid)){
            return null;
        }
        return uid;
    }
    //查询令牌的有效期
    public long getExpire(String access_token){
        //key
        String key = "user_token:"+access_token;
        Long expire = stringRedisTemplate.getExpire(key,TimeUnit.SECONDS);
        return expire;
    }

}
