package com.xuecheng.manage_cms_client.service;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.CmsSite;
import com.xuecheng.manage_cms_client.dao.CmsPageRepository;
import com.xuecheng.manage_cms_client.dao.CmsSiteRepository;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import sun.nio.ch.IOUtil;

import java.io.*;
import java.util.Optional;

/**
 * @Classname PageService
 * @Description TODO
 * @Date 2021/7/31 15:05
 * @Author by YeLiu
 */
@Service
public class PageService {
    private static final Logger LOGGER= LoggerFactory.getLogger(PageService.class);
    @Autowired
    GridFsTemplate gridFsTemplate;
    @Autowired
    GridFSBucket gridFSBucket;
    @Autowired
    CmsPageRepository cmsPageRepository;
    @Autowired
    CmsSiteRepository cmsSiteRepository;
    public void savePageToServerPath(String pageId){
        //根据pageId查询到CmsPage
        CmsPage cmsPage = this.findCmsPageByPageId(pageId);
        //根据CmsPage查询到HtmlFileID
        String htmlFileId = cmsPage.getHtmlFileId();
        //从GridFs查询html文件
        InputStream inputStream = this.getFileById(htmlFileId);
        if(inputStream == null){
            LOGGER.error("getFileById InputStream is null htmlFileId:{}"+htmlFileId);
        }
        //得到站点物理路径
        String siteId = cmsPage.getSiteId();
        CmsSite cmsSite = this.findCmsSiteByPageId(siteId);
        //得到页面物理路径
        String sitePhysicalPath = cmsSite.getSitePhysicalPath();
        //将html文件保存到物理路径上
        String pagePath=sitePhysicalPath+cmsPage.getPagePhysicalPath()+cmsPage.getPageName();
        FileOutputStream fileOutputStream=null;
        try {
             fileOutputStream=new FileOutputStream(new File(pagePath));
            IOUtils.copy(inputStream, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
   public InputStream getFileById(String htmlFileId){
       GridFSFile gridFSFiles = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(htmlFileId)));
       GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(gridFSFiles.getObjectId());
       GridFsResource gridFsResource=new GridFsResource(gridFSFiles,gridFSDownloadStream);
       try {
           return gridFsResource.getInputStream();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return null;
   }

   /**
    * @Description 根据页面id查询页面信息
    * @param PageId
    * @return com.xuecheng.framework.domain.cms.CmsPage
    * @date 2021/7/31 15:14
    * @auther YeLiu
    */
    public CmsPage findCmsPageByPageId(String PageId){
        Optional<CmsPage> op = cmsPageRepository.findById(PageId);
        if(op.isPresent()){
            return op.get();
        }
        return  null;
    }

    public CmsSite findCmsSiteByPageId(String siteId){
        Optional<CmsSite> op = cmsSiteRepository.findById(siteId);
        if(op.isPresent()){
            return op.get();
        }
        return  null;
    }
}
