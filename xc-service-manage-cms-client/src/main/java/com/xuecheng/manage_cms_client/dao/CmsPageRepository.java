package com.xuecheng.manage_cms_client.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Classname CmsPageRepository
 * @Description TODO
 * @Date 2021/7/17 11:19
 * @Author by YeLiu
 */
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {

}
